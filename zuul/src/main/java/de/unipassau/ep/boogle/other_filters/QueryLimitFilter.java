package de.unipassau.ep.boogle.other_filters;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.unipassau.ep.boogle.security.SecurityConstants.AUTH_HEADER;

/**
 * This filter keeps track of the amount of sparql of fulltext requests made by logged out users.
 */
@Component
@EnableScheduling
public class QueryLimitFilter extends ZuulFilter {

    private int queryLimit = 0;

    /**
     * to classify a filter by type. Standard types in Zuul are "pre" for pre-routing filtering,
     * "route" for routing to an origin, "post" for post-routing filters, "error" for error handling.
     * We also support a "static" type for static responses see  StaticResponseFilter.
     * Any filterType made be created or added and run by calling FilterProcessor.runFilters(type)
     *
     * @return A String representing that type
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * filterOrder() must also be defined for a filter. Filters may have the same  filterOrder if precedence is not
     * important for a filter. filterOrders do not need to be sequential.
     *
     * @return the int order of a filter
     */
    @Override
    public int filterOrder() {
        return 3;
    }

    /**
     * a "true" return from this method means that the run() method should be invoked
     *
     * @return true if the run() method should be invoked. false will not invoke the run() method
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * Checks if the user is logged out and performs a query of some sort (sparql or fulltext).
     * If he does, the counter gets incremented by one.
     * When the counter reaches 100, no more requests of that sort will be possible.
     * The counter will be reset to 0 after every passed minute by {@link QueryLimitFilter#resetCounter()}.
     *
     * @return Some arbitrary artifact may be returned. Current implementation ignores it.
     */
    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpServletResponse response = ctx.getResponse();

        String url = request.getRequestURI();
        String header = request.getHeader(AUTH_HEADER);
        if (header == null || header.equals("")) { // means unauthenticated, and therefore logged out
            if ((url.equals("/service-query/api/sparql") || url.equals("/service-query/api/fulltext"))) {
                if (queryLimit >= 100) {
                    ctx.setSendZuulResponse(false);
                    try {
                        response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Too many requests");
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    queryLimit++;
                }
            }
        }
        return null;
    }

    /**
     * Resets the queryLimit after every minute.
     */
    @Scheduled(fixedRate = 1000 * 60)
    private void resetCounter() {
        queryLimit = 0;
    }
}
