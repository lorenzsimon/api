package de.unipassau.ep.boogle.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static de.unipassau.ep.boogle.security.SecurityConstants.*;

/**
 * This filter class makes sure that the id in the token is the same as the one in the url. If not, a 401 Unauthorized
 * is returned. Else, the request gets routed to its destination.
 */
public class IDFilter extends ZuulFilter {

    /**
     * {@inheritDoc}
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int filterOrder() {
        return 2;
    }

    /**
     * a "true" return from this method means that the run() method should be invoked
     *
     * @return true if the run() method should be invoked. false will not invoke the run() method
     */
    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        return (boolean) request.getAttribute("continue");
    }

    /**
     * If the request is based on an id, it is made sure that the id matches the one signed in the jwt. If not, the
     * request ends here and 401 is returned to the client.
     *
     * @return Returns null.
     */
    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpServletResponse response = ctx.getResponse();

        String header = request.getHeader(AUTH_HEADER);
        String url = request.getRequestURI();

        boolean isAdmin = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(header.replace(TOKEN_PREFIX, ""))
                .getClaim("isAdmin").asBoolean();

        if (url != null && url.contains("/user/") && !isIdValid(url, header) && !isAdmin) { // no check for admins

            // stops the request from further execution in the destination service
            ctx.setSendZuulResponse(false);

            try {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "ID not valid");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    /**
     * Compares the id from the token with the one sent in the url.
     *
     * @param url The url containing the id.
     * @param token The containing containing the valid id.
     * @return Returns true if the id from the url is valid, and false if not.
     */
    private boolean isIdValid(String url, String token) {

        int actualID = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(token.replace(TOKEN_PREFIX, ""))
                .getClaim("id").asInt();

        String beforeIDRegex = "\\S*\\b/user/\\b";
        String afterIDRegex = "\\D+.*";
        url = url.replaceAll(beforeIDRegex, "");
        url = url.replaceAll(afterIDRegex, "");

        int id = Integer.parseInt(url);
        return id == actualID;
    }
}
