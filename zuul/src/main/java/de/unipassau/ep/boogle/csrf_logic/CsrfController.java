package de.unipassau.ep.boogle.csrf_logic;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import de.unipassau.ep.boogle.utility.JSONBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.unipassau.ep.boogle.security.SecurityConstants.SECRET_KEY;
import static de.unipassau.ep.boogle.security.SecurityConstants.TOKEN_PREFIX;

/**
 * This Controller class manages the /csrf endpoint.
 */
@RestController
public class CsrfController {

    @Autowired
    private JSONBuilder jsonBuilder;

    @Autowired
    private CsrfService csrfService;

    /**
     * This will trigger the filter chain, as always. However, it's required to break the chain for this specific
     * request, as it mysteriously causes a 500 Access Denied instead of the expected 403 Forbidden.
     */
    @GetMapping("/csrf")
    public void getCsrfToken() {

    }

    /**
     * This method actually performs the logic of the /csrf mapping. It is called after the access token has been
     * checked for validity. If it is not valid, a 403 is returned, however without a body, because spring boot does not
     * want to carry over the sendError method from the Authorization filter for some reason.
     *
     * @param header The access token.
     * @param res The http response.
     * @throws IOException Thrown if something goes wrong.
     */
    public void bypassFilterChain(String header, HttpServletResponse res) throws IOException {
        try {
            int id = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                    .build()
                    .verify(header.replace(TOKEN_PREFIX, ""))
                    .getClaim("id").asInt();

            csrfService.addTokenByID(id);
            String response = jsonBuilder.build()
                    .setAdditionalKeyValuePair("csrf_token", csrfService.getTokenByID(id))
                    .toJSON();

            res.getWriter().write(response);
            res.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception ignored) {
        }
    }
}