package de.unipassau.ep.boogle.security;

/**
 * This class contains important constants that should not be changed (except maybe TTL for security optimizations).
 */
public class SecurityConstants {
    public static final String SECRET_KEY = "please1don't2tell3anyone4about5this!";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUTH_HEADER = "Authorization";
}
