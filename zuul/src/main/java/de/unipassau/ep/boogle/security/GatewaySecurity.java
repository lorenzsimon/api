package de.unipassau.ep.boogle.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

/**
 * This security config class applies the authorization filter to protected endpoints, so that only authorized users can
 * access them.
 */
@Configuration
@EnableWebSecurity
public class GatewaySecurity extends WebSecurityConfigurerAdapter {

    /**
     * Configures Spring's Security to fit our needs. Details are in the README of the application.
     *
     * @param http The {@link HttpSecurity} object that is getting configured.
     * @throws Exception Throws an Exception in case something goes wrong.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.requestMatchers().and().cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/service-auth/delete_admin").hasAuthority("ROLE_ADMIN")
                .antMatchers("/service-query/api/queries/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                .antMatchers("/service-auth/**", "/service-query/**")
                .permitAll()

                .antMatchers("/service-logging/**")
                .hasAuthority("ROLE_ADMIN")

                .antMatchers("/service-user/**", "/service-discussion/**")
                .hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")

                .anyRequest().authenticated()
                .and()
                .addFilter(new AuthorizationFilter(authenticationManager()))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
