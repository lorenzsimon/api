package de.unipassau.ep.boogle.csrf_logic;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * This Service class handles the csrf tokens of different users.
 */
@Service
public class CsrfService {

    private static final int CSRF_TOKEN_LENGTH = 128;

    /**
     * A HashMap is needed here to account for different users making requests simultaneously.
     * The id is used instead of the access token as a whole, since the token changes, but the id stays the same.
     */
    private final HashMap<Integer, String> csrfTokenByID = new HashMap<>();

    /**
     * Sets a new random csrf string for the given id.
     * @param id The id of the user.
     */
    public void addTokenByID(int id) {
        csrfTokenByID.put(id, RandomStringUtils.randomAlphanumeric(CSRF_TOKEN_LENGTH));
    }

    /**
     * Gets the currently valid csrf token for the user with the given id.
     * @param id The id of the user.
     *
     * @return Returns the csrf token if it exists for the given key, null else.
     */
    public String getTokenByID(int id) {
        return csrfTokenByID.get(id);
    }
}
