package de.unipassau.ep.boogle.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import de.unipassau.ep.boogle.csrf_logic.CsrfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.unipassau.ep.boogle.security.SecurityConstants.SECRET_KEY;
import static de.unipassau.ep.boogle.security.SecurityConstants.TOKEN_PREFIX;

/**
 * A custom implementation of the csrf token protection mechanism. Since the frontend is running on another port, we
 * cannot simply use Spring-Boot's magic to handle csrf protection. This filter/controller implements its own csrf
 * checking mechanism.
 */
@RestController // better as @Component, but i don't want to change this now anymore (too risky)
public class CustomCsrfFilter extends ZuulFilter {

    @Autowired
    private CsrfService csrfService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int filterOrder() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        return (boolean) request.getAttribute("continue") && !request.getMethod().equals("GET");
    }

    /**
     * Checks if the given csrf token matches the expected token from the hashmap.
     * If the tokens match, a new one will be generated for this user, and if not, the request returns a 403 to the
     * frontend without a message, because we want to keep our csrf check as secret as possible.
     *
     * @return Returns null.
     */
    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        HttpServletResponse response = ctx.getResponse();
        String csrfTokenHeader = request.getHeader("csrf");
        String accessTokenHeader = request.getHeader("Authorization");

        int id = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(accessTokenHeader.replace(TOKEN_PREFIX, ""))
                .getClaim("id").asInt();

        if (csrfTokenHeader == null || csrfService.getTokenByID(id) == null
                || !csrfService.getTokenByID(id).equals(csrfTokenHeader)) {
            ctx.setSendZuulResponse(false);
            request.setAttribute("continue", false);
            try {
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            csrfService.addTokenByID(id); // invalidates the old token by creating a new one
        }
        return null;
    }
}
