package de.unipassau.ep.boogle;

import de.unipassau.ep.boogle.security.IDFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * The entrypoint of the gateway. Every request is sent through and redirected by this service.
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ZuulGatewayApplication {

	/**
	 * Starts up the gateway.
	 *
	 * @param args Unused.
	 */
	public static void main(String[] args) {
		SpringApplication.run(ZuulGatewayApplication.class, args);
	}

	/**
	 * Configures cors to allow requests from our frontend.
	 *
	 * @return Returns the configured filter.
	 */
	@Bean
	@Primary
	public CorsFilter corsFilter() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		final CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("https://ep-2020-3.dimis.fim.uni-passau.de");
		config.addAllowedHeader("*");
		config.addAllowedMethod("OPTIONS");
		config.addAllowedMethod("HEAD");
		config.addAllowedMethod("GET");
		config.addAllowedMethod("PUT");
		config.addAllowedMethod("POST");
		config.addAllowedMethod("DELETE");
		config.addAllowedMethod("PATCH");
		source.registerCorsConfiguration("/**", config);
		return new CorsFilter(source);
	}

	/**
	 * Creates bean for the IDFilter.
	 *
	 * @return Returns a new IDFilter.
	 */
	@Bean
	public IDFilter simpleFilter() {
		return new IDFilter();
	}
}