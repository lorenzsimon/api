package de.unipassau.ep.boogle.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.netflix.zuul.context.RequestContext;
import de.unipassau.ep.boogle.csrf_logic.CsrfController;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static de.unipassau.ep.boogle.security.SecurityConstants.*;

/**
 * This filter class handles authorization, and therefore roles.
 * While authentication is handled by the auth-service, authorization is taken care of by the gateway.
 */
@Order(0)
public class AuthorizationFilter extends BasicAuthenticationFilter {

    private CsrfController csrfController;

    /**
     * Sets the {@link AuthenticationManager} for this filter.
     * @param authManager The authManager for this filter.
     */
    public AuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    /**
     * Authorizes the user if the provided authorization header is valid.
     *
     * @param req The incoming request.
     * @param res The response sent to the client.
     * @param chain The FilterChain, used to forward to the next filter.
     * @throws IOException Thrown in case something goes wrong when accessing the input stream of the request.
     * @throws ServletException Thrown in case something goes wrong with the servlet.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException, AuthenticationException {
        if (csrfController == null)
            initController(req);

        req.setAttribute("continue", true);
        String header = req.getHeader(AUTH_HEADER);

        // if no Authorization header is present, or the value of its key doesn't start with "Bearer ",
        // the method calls the next filter
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            req.setAttribute("continue", false);
            chain.doFilter(req, res);
            return;
        }

        // verifies the JWT
        UsernamePasswordAuthenticationToken authentication = getAuthentication(header, req, res);

        // gives the JWT to Springs SecurityContext
        SecurityContextHolder.getContext().setAuthentication(authentication);
        if (req.getRequestURI().equals("/csrf")) {
            req.setAttribute("continue", false);
            csrfController.bypassFilterChain(header, res);
        } else {
            chain.doFilter(req, res);
        }
    }

    /**
     * Checks the validity of the token.
     * If the token is valid, the user gets their authority (which is either ROLE_USER or ROLE_ADMIN).
     * Expired tokens are recognized and an error is sent to the client.
     * By verifying the existence of the isAdmin claim, it is also checked if the token is in fact an access token, and
     * if it is not, an error is sent also sent to client.
     *
     * @param header The Authorization header containing the Bearer Token if it isn't null.
     * @return Returns the Token containing the authentication for the user, including possible roles.
     */
    private UsernamePasswordAuthenticationToken getAuthentication(String header, HttpServletRequest req,
                                                                  HttpServletResponse res) throws IOException {
        if (header != null) {
            try {
                String user = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                        .build()
                        .verify(header.replace(TOKEN_PREFIX, ""))
                        .getSubject();

                // decoding of the claim that specifies the role of the user
                boolean isAdmin = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                        .build()
                        .verify(header.replace(TOKEN_PREFIX, ""))
                        .getClaim("isAdmin").asBoolean();

                // framework requires a collection
                Collection<SimpleGrantedAuthority> role = new ArrayList<>();
                if (isAdmin) {
                    role.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                } else {
                    role.add(new SimpleGrantedAuthority("ROLE_USER"));
                }

                if (user != null) {
                    return new UsernamePasswordAuthenticationToken(user, null, role);
                }

            } catch (NullPointerException e) {
                RequestContext ctx = RequestContext.getCurrentContext();
                ctx.setSendZuulResponse(false);
                res.sendError(HttpServletResponse.SC_FORBIDDEN, "Not an access token");
                req.setAttribute("continue", false);
            }
            catch (TokenExpiredException e) {
                RequestContext ctx = RequestContext.getCurrentContext();
                ctx.setSendZuulResponse(false);
                res.sendError(HttpServletResponse.SC_FORBIDDEN, "Token expired");
                req.setAttribute("continue", false);
            }
        }
        return null;
    }

    /**
     * Lazily initializes the Components on the first call, as they cannot be autowired in a filter class without
     * further configuration.
     *
     * @param req The request that defines the servlet context.
     */
    private void initController(HttpServletRequest req) {
        ServletContext servletContext = req.getServletContext();
        WebApplicationContext webApplicationContext =
                WebApplicationContextUtils.getWebApplicationContext(servletContext);
        assert webApplicationContext != null;
        if (csrfController == null) {
            csrfController = webApplicationContext.getBean(CsrfController.class);
        }
    }
}