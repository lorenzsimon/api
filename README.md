# Documentation of the API:

## What is the main purpose of the API?

The API Gateway is the single point of access to our microservice architecture. (Almost) Every request is routed through the filters of the Gateway before it gets redirected and executed in the repsonsible microservice. The only exception is the /csrf mapping, which is answered from the Gateway directly.
The API is documented here: https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/api/-/wikis/Zuul-API

## Internal Service Logic:

The Gateway is a ZuulProxy, and together with the discovery service (which uses Eureka), it is able to resolve the service names in mappings. So if a request to /service-auth is made, the api realizes that it has to send the request to the auth-service, without having to specify the port. It also uses a variety of filters to handle cross-cutting security concerns and user-tracking for unauthenticated users. Most of the configuration is done by Spring automatically, which makes routing very easy.   
NOTE: If one of the filters fails, the chain ends and no request is redirected to any backend service. The client gets an error.

## The filters (in order of execution)
#### AuthorizationFilter
While the auth-service handles authentication, this filter checks the tokens for validity and assigns authorities (roles) based on the isAdmin flag in the payload of the jwt. If there is no access token present in the header, this and the following filters get skipped, and users won't be able to use mappings exclusive to authenticated users. If the called mapping is /csrf and an access token is present, the following filters get skipped as well, but the request is executed and a csrf token is returned to the client. For more information on the /csrf mapping, it is best to take a look at the code documentation, where it is documented quite thoroughly why it has to be done this way.

#### CustomCsrfFilter
Csrf tokens are essential to prevent csrf attacks. When a user with a valid, non expired access token calls /csrf, a csrf token for this user will be returned. At the next request that requires authorization (excluding GET requests), the csrf header gets compared to the token that has been issued to the user at the /csrf request, and if the two match, the old csrf is now invalid, and for the next request, a new one has to be requested first.
The /csrf mapping (which is a GET mapping) and all other GET mappings are safe, since the correctly configured cors policy only allows for our frontend to read the responses coming from the backend.

#### IDFilter
The IDFilter makes sure that users can only receive data of their own account. So if the user with id 3 wants to get data for user 8, a 401 Unauthorized is returned. For this, the id from the url is compared with the one in the payload of the access token. The url has to follow this pattern: .../user/{id}(/...) .

#### QueryLimitFilter
The QueryLimitFilter is our way of tracking unauthenticated users. When there are more than 100 requests from unauthenticated users during one minute of uptime of the Gateway, a 503 Service Unavailable is returned to the client. After the minute is over, the request counter resets, and logged off users can make requests again.

## The GatewaySecurity Class
This class configures Spring-Boot's Security. The first line (http.requestMatchers().and().cors().and().csrf().disable()) configures Spring to use cors and to ignore its own internal csrf handling (since we have our own implementation). The antMatchers select mappings and assign access control to these. The hasAuthority method means that only users with this authority can access the endpoint. 
More information about roles can be found here: https://git.fim.uni-passau.de/ep/ws20_21/team_dis_1/api/-/wikis/roles

## Known Bugs:
If a token is expired and the called mapping is /csrf, the answer is not a 403 with message "Token expired" as expected. In this case, only the status code 403 is returned, with an empty body.

The IDFilter currently only checks if the id from the request matches the id from the jwt if the mapping follows the .../user/{id}(/...) pattern. This security check might be required for more than these mappings, however the mappings are not consistent enough accross the services to apply more regular expressions to the filter.

Some mappings might not have been configured for roles yet (means, that mappings for admins could be called by users). In that case, the role check can be added in the GatewaySecurity class with a one liner similar to the entries that are already present.

After validating a csrf token, the token value for the id in the csrf HashMap does not get deleted, but a new one is generated. This is still almost impossible to guess, but not having a token at all would be better.